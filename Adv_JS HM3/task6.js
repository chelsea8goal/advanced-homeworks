
    /**
    * Задание 6
    * Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть
    * создан новый объект, который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.
    */
    const employee = {
    name3: 'Vitalii',
    surname: 'Klichko'
}
    let {name3, surname, age = 30, salary = 20000} = employee;

    let newEmployee = {name3, surname, age, salary};

    console.log('Sixth task');
    console.log(newEmployee);

