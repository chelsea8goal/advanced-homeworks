package com.example.predictor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.lights.Light;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Spinner spinner, spinner2;
    public EditText chel_score, away_score, away_res, chel_res, chel_min, res_min,chel_shoot,shoot_res,chel_ongoal,res_ongoal, chel_pos,res_pos, chel_corner,res_corner;
    TextView t1, goalsRes, playerRes, minRes, shootRes, ongoalRes, posRes, cornerRes;
    CheckBox scored;
    ImageButton reset;
    int  chelGoalNum=0, awayGoalNum=0, chelGoalRes=0, awayGoalRes=0, spinerPos,chelMinNum=0,chelMinRes=0,chelShootNum,chelShootRes, chelOngoalNum, chelOngoalRes,chelPosNum,chelPosRes,chelCornerRes, chelCornerNum;
    int goalsSum = 0;
    int playerSum = 0;
    int minutesSum = 0;
    int shootSum =0;
    int ongoalSum=0;
    int posSum = 0;
    int cornerSum = 0;
    int finalSum = 0;
    String s1="0", s2="0", s3="0",s3res="0", s1res="0", s2res="0", item, item2,s4,s4res,s5,s5res,s6,s6res,s7,s7res,s8,s8res,s9,s9res,s10,s10res;
    SharedPreferences sPref, sPref2, sPref3, sPref4, sPref5, sPref6, sPref7,sPref8,sPref9,sPref10 ;
    final String SAVED_chel_score = "saved_text";
    final String SAVED_away_score = "saved_text";
    final String SAVED_chel_res = "saved_text";
    final String SAVED_away_res = "saved_text";
    final String SAVED_chel_min = "saved_text";
    final String SAVED_chel_shoot = "saved_text";
    final String SAVED_chel_ongoal = "saved_text";
    final String SAVED_chel_pos = "saved_text";
    final String SAVED_chel_corners = "saved_text";

    int SAVED_chel_player;


    String[] players = {"Player","Kepa", "Mendy", "James", "Azpilicueta", "Silva", "Chalobah","Chilwell","Koulibaly"
    ,"Cucurella","Fofana","Jorginho","Kante","Kovacic","Loftus-Cheek","Mount","Zakaria","Gallagher",
    "Chukwuemeka","Aubameyang","Pulisic","Sterling","Broja","Ziyech","Havertz"};



    public void saveText(View v) {
        sPref = getSharedPreferences("MyPref", MODE_PRIVATE);
        sPref2 = getSharedPreferences("MyPref2", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        SharedPreferences.Editor ed2 = sPref2.edit();
        ed.putString(SAVED_chel_score, chel_score.getText().toString());
        ed2.putString(SAVED_away_score, away_score.getText().toString());
        ed.commit();
        ed2.commit();

        sPref3 = getSharedPreferences("MyPref3", MODE_PRIVATE);
        SharedPreferences.Editor ed3 = sPref3.edit();
        ed3.putInt(String.valueOf(SAVED_chel_player), spinerPos);
        ed3.commit();

        sPref4 = getSharedPreferences("MyPref4", MODE_PRIVATE);
        SharedPreferences.Editor ed4 = sPref4.edit();
        ed4.putString(SAVED_chel_res, chel_res.getText().toString());
        ed4.commit();
        sPref5 = getSharedPreferences("MyPref5", MODE_PRIVATE);
        SharedPreferences.Editor ed5 = sPref5.edit();
        ed5.putString(SAVED_away_res, away_res.getText().toString());
        ed5.commit();

        sPref6 = getSharedPreferences("MyPref6", MODE_PRIVATE);
        SharedPreferences.Editor ed6 = sPref6.edit();
        ed6.putString(SAVED_chel_min, chel_min.getText().toString());
        ed6.commit();
        sPref7 = getSharedPreferences("MyPref7", MODE_PRIVATE);
        SharedPreferences.Editor ed7 = sPref7.edit();
        ed7.putString(SAVED_chel_shoot, chel_shoot.getText().toString());
        ed7.commit();

        sPref8 = getSharedPreferences("MyPref8", MODE_PRIVATE);
        SharedPreferences.Editor ed8 = sPref8.edit();
        ed8.putString(SAVED_chel_ongoal, chel_ongoal.getText().toString());
        ed8.commit();
        sPref9 = getSharedPreferences("MyPref9", MODE_PRIVATE);
        SharedPreferences.Editor ed9 = sPref9.edit();
        ed9.putString(SAVED_chel_pos, chel_pos.getText().toString());
        ed9.commit();
        sPref10 = getSharedPreferences("MyPref10", MODE_PRIVATE);
        SharedPreferences.Editor ed10 = sPref10.edit();
        ed10.putString(SAVED_chel_corners, chel_corner.getText().toString());
        ed10.commit();

       Toast.makeText(MainActivity.this, "Text saved", Toast.LENGTH_SHORT).show();
       // goalsRes.setText(SAVED_chel_score+SAVED_away_score);


    }
    public void loadText() {
        sPref = getSharedPreferences("MyPref", MODE_PRIVATE);
        sPref2 = getSharedPreferences("MyPref2", MODE_PRIVATE);
        String savedText = sPref.getString(SAVED_chel_score, "");
        String savedText2 = sPref2.getString(SAVED_away_score, "");
        chel_score.setText(savedText);
        away_score.setText(savedText2);

        sPref3 = getSharedPreferences("MyPref3", MODE_PRIVATE);
        spinerPos = sPref3.getInt(String.valueOf(SAVED_chel_player), 0);
        spinner.setSelection(spinerPos);

        sPref4 = getSharedPreferences("MyPref4", MODE_PRIVATE);
        String savedText4 = sPref4.getString(SAVED_chel_res, "");
        chel_res.setText(savedText4);
        sPref5 = getSharedPreferences("MyPref5", MODE_PRIVATE);
        String savedText5 = sPref5.getString(SAVED_away_res, "");
        away_res.setText(savedText5);

        sPref6 = getSharedPreferences("MyPref6", MODE_PRIVATE);
        String savedText6 = sPref6.getString(SAVED_chel_min, "");
        chel_min.setText(savedText6);
        sPref7 = getSharedPreferences("MyPref7", MODE_PRIVATE);
        String savedText7 = sPref7.getString(SAVED_chel_shoot, "");
        chel_shoot.setText(savedText7);
        sPref8 = getSharedPreferences("MyPref8", MODE_PRIVATE);
        String savedText8 = sPref8.getString(SAVED_chel_ongoal, "");
        chel_ongoal.setText(savedText8);
        sPref9 = getSharedPreferences("MyPref9", MODE_PRIVATE);
        String savedText9 = sPref9.getString(SAVED_chel_pos, "");
        chel_pos.setText(savedText9);
        sPref10 = getSharedPreferences("MyPref10", MODE_PRIVATE);
        String savedText10 = sPref10.getString(SAVED_chel_corners, "");
        chel_corner.setText(savedText10);

        // Toast.makeText(MainActivity.this, "Text loaded", Toast.LENGTH_SHORT).show();
        //goalsRes.setText(savedText+savedText2);
    }

    public void pointsPage(View v){
        Intent intent = new Intent(this, Points.class);
        startActivity(intent);
    }
    public void clear(View v){
        chel_score.getText().clear();
        away_score.getText().clear();
        chel_res.getText().clear();
        away_res.getText().clear();
        spinner.setSelection(0);
        spinner2.setSelection(0);
        chel_min.getText().clear();
        res_min.getText().clear();
        chel_shoot.getText().clear();
        shoot_res.getText().clear();
        chel_ongoal.getText().clear();
        res_ongoal.getText().clear();
        chel_pos.getText().clear();
        res_pos.getText().clear();
        chel_corner.getText().clear();
        res_corner.getText().clear();
        cornerRes.setText("");
        posRes.setText("");
        ongoalRes.setText("");
        shootRes.setText("");
        minRes.setText("");
        playerRes.setText("");
        goalsRes.setText("");
        t1.setText("");
    }
    public void goalRes(View v){
        s1 = chel_score.getText().toString();
        s2 = away_score.getText().toString();
        s1res = chel_res.getText().toString();
        s2res = away_res.getText().toString();

        s3 = chel_min.getText().toString();
        s3res = res_min.getText().toString();
        s4 = chel_shoot.getText().toString();
        s4res = shoot_res.getText().toString();
        s5 = chel_ongoal.getText().toString();
        s5res = res_ongoal.getText().toString();
        s6 = chel_pos.getText().toString();
        s6res = res_pos.getText().toString();

        s7 = chel_corner.getText().toString();
        s7res = res_corner.getText().toString();


        // converting string to int.
        chelGoalNum = Integer.parseInt(s1);
        awayGoalNum = Integer.parseInt(s2);
        chelGoalRes = Integer.parseInt(s1res);
        awayGoalRes = Integer.parseInt(s2res);
        chelMinNum = Integer.parseInt(s3);
        chelMinRes = Integer.parseInt(s3res);
        chelShootNum = Integer.parseInt(s4);
        chelShootRes = Integer.parseInt(s4res);
        chelOngoalNum = Integer.parseInt(s5);
        chelOngoalRes = Integer.parseInt(s5res);
        chelPosNum = Integer.parseInt(s6);
        chelPosRes = Integer.parseInt(s6res);
        chelCornerNum = Integer.parseInt(s7);
        chelCornerRes = Integer.parseInt(s7res);

       if((chelGoalNum == chelGoalRes) && (awayGoalNum == awayGoalRes)){
            goalsSum=25;
        }
        else if((chelGoalNum == chelGoalRes) || (awayGoalNum == awayGoalRes)){
            goalsSum=10;
           if((chelGoalNum>awayGoalNum)&&(chelGoalRes>awayGoalRes)){
               goalsSum+=5;
           }
        }
        else if((chelGoalNum>awayGoalNum)&&(chelGoalRes>awayGoalRes)){
            goalsSum=5;
        }
        else if((chelGoalNum==awayGoalNum)&&(chelGoalRes==awayGoalRes)){
            goalsSum=5;
       }
        else{
            goalsSum=0;
        }

        if(chelMinNum==chelMinRes){
            minutesSum=20;
        }
        else if((chelMinNum+1==chelMinRes)||(chelMinNum-1==chelMinRes)){
            minutesSum=10;
        }
        else if((chelMinNum+2==chelMinRes)||(chelMinNum-2==chelMinRes)){
            minutesSum=9;
        }
        else if((chelMinNum+3==chelMinRes)||(chelMinNum-3==chelMinRes)){
            minutesSum=8;
        }
        else if((chelMinNum+4==chelMinRes)||(chelMinNum-4==chelMinRes)){
            minutesSum=7;
        }
        else if((chelMinNum+5==chelMinRes)||(chelMinNum-5==chelMinRes)){
            minutesSum=6;
        }
        else if((chelMinNum+6==chelMinRes)||(chelMinNum-6==chelMinRes)){
            minutesSum=5;
        }
        else if((chelMinNum+7==chelMinRes)||(chelMinNum-7==chelMinRes)){
            minutesSum=4;
        }
        else if((chelMinNum+8==chelMinRes)||(chelMinNum-8==chelMinRes)){
            minutesSum=3;
        }
        else if((chelMinNum+9==chelMinRes)||(chelMinNum-9==chelMinRes)){
            minutesSum=2;
        }
        else if((chelMinNum+10==chelMinRes)||(chelMinNum-10==chelMinRes)){
            minutesSum=1;
        }
        else{
            minutesSum=0;
        }

        if(chelShootNum==chelShootRes){
            shootSum=10;
        }
        else if((chelShootNum+1==chelShootRes)||(chelShootNum-1==chelShootRes)){
            shootSum=5;
        }
        else if((chelShootNum+2==chelShootRes)||(chelShootNum-2==chelShootRes)){
            shootSum=3;
        }
        else if((chelShootNum+3==chelShootRes)||(chelShootNum-3==chelShootRes)){
            shootSum=1;
        }
        else{
            shootSum =0;
        }
//on goal
        if(chelOngoalNum==chelOngoalRes){
            ongoalSum=10;
        }
        else if((chelOngoalNum+1==chelOngoalRes)||(chelOngoalNum-1==chelOngoalRes)){
            ongoalSum=5;
        }
        else if((chelOngoalNum+2==chelOngoalRes)||(chelOngoalNum-2==chelOngoalRes)){
            ongoalSum=3;
        }
        else if((chelOngoalNum+3==chelOngoalRes)||(chelOngoalNum-3==chelOngoalRes)){
            ongoalSum=1;
        }
        else{
            ongoalSum =0;
        }
//possesion
        if(chelPosNum==chelPosRes){
            posSum=10;
        }
        else if((chelPosNum+1==chelPosRes)||(chelPosNum-1==chelPosRes)){
            posSum=5;
        }
        else if((chelPosNum+2==chelPosRes)||(chelPosNum-2==chelPosRes)){
            posSum=4;
        }
        else if((chelPosNum+3==chelPosRes)||(chelPosNum-3==chelPosRes)){
            posSum=3;
        }
        else if((chelPosNum+4==chelPosRes)||(chelPosNum-4==chelPosRes)){
            posSum=2;
        }
        else if((chelPosNum+5==chelPosRes)||(chelPosNum-5==chelPosRes)){
            posSum=1;
        }
        else{
            posSum=0;
        }
        //corners
        if(chelCornerNum==chelCornerRes){
            cornerSum=10;
        }
        else if((chelCornerNum+1==chelCornerRes)||(chelCornerNum-1==chelCornerRes)){
            cornerSum=5;
        }
        else if((chelCornerNum+2==chelCornerRes)||(chelCornerNum-2==chelCornerRes)){
            cornerSum=3;
        }
        else if((chelCornerNum+3==chelCornerRes)||(chelCornerNum-3==chelCornerRes)){
            cornerSum=1;
        }
        else{
            cornerSum =0;
        }

        if(scored.isChecked()){
            playerSum=5;
        }



        goalsRes.setText("Points: " +Integer.toString(goalsSum));
        playerRes.setText("Points: "+Integer.toString(playerSum));
        minRes.setText("Points: "+Integer.toString(minutesSum));
        shootRes.setText("Points: "+Integer.toString(shootSum));
        ongoalRes.setText("Points: "+Integer.toString(ongoalSum));
        posRes.setText("Points: "+Integer.toString(posSum));
        cornerRes.setText("Points: "+Integer.toString(cornerSum));

        finalSum = goalsSum+playerSum+minutesSum+shootSum+ongoalSum+posSum+cornerSum;
        t1.setText("Your points: " + Integer.toString(finalSum));


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);
        chel_score = (EditText) findViewById(R.id.chel_score);
        away_score = (EditText) findViewById(R.id.away_score);
        chel_shoot = (EditText) findViewById(R.id.num7);
        shoot_res = (EditText) findViewById(R.id.num10);
        reset = (ImageButton) findViewById(R.id.imageButton);
        scored = (CheckBox) findViewById(R.id.checkBox);

        chel_min = (EditText) findViewById(R.id.num3);
        res_min = (EditText) findViewById(R.id.num14);

        chel_ongoal = (EditText) findViewById(R.id.num4);
        res_ongoal = (EditText) findViewById(R.id.num13);

        chel_pos = (EditText) findViewById(R.id.num5);
        res_pos = (EditText) findViewById(R.id.num12);
        chel_corner = (EditText) findViewById(R.id.num6);
        res_corner = (EditText) findViewById(R.id.num9);

        t1 = (TextView) findViewById(R.id.result);
        goalsRes = (TextView) findViewById(R.id.goalsRes);
        playerRes = (TextView) findViewById(R.id.playerRes);
        minRes = (TextView) findViewById(R.id.minRes);
        shootRes = (TextView) findViewById(R.id.shootRes);
        ongoalRes = (TextView) findViewById(R.id.ongoalRes);
        posRes = (TextView) findViewById(R.id.posRes);
        cornerRes = (TextView) findViewById(R.id.cornerRes);

        chel_res = (EditText) findViewById(R.id.chel_res);
        away_res = (EditText) findViewById(R.id.away_res);
       spinner = findViewById(R.id.spinner);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> adapter = new ArrayAdapter(this, R.layout.spiner_design, players);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        spinner.setAdapter(adapter);

        spinner2 = findViewById(R.id.spinner2);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> adapter2 = new ArrayAdapter(this, R.layout.spiner_design2, players);
        // Определяем разметку для использования при выборе элемента
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        spinner2.setAdapter(adapter2);

        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = (String)parent.getItemAtPosition(position);
                spinerPos = adapter.getPosition(item);
                AdapterView.OnItemSelectedListener itemSelectedListener2 = new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent2, View view2, int position2, long id2) {

                        // Получаем выбранный объект
                        item2 = (String)parent2.getItemAtPosition(position2);
                        if(item.equals(item2)){
                            playerSum = 20;
                        }
                        else{
                            playerSum = 0;
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent2) {
                    }
                };
                spinner2.setOnItemSelectedListener(itemSelectedListener2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        };
        spinner.setOnItemSelectedListener(itemSelectedListener);
        loadText();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
      // saveText();
    }
    }

