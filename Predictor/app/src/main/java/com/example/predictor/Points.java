package com.example.predictor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Points extends AppCompatActivity {
    Button button;
    WebView web;
    EditText editText;
    ArrayList list;
    TextView pointsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points);

        list = new ArrayList<String>();
        web = findViewById(R.id.webView);
        // Create a WebSettings object
        WebSettings webSettings = web.getSettings();
        // For using Zoom feature
        // webSettings.setBuiltInZoomControls(true);
        // Enable JavaScript using the WebSettings object
        webSettings.setJavaScriptEnabled(true);
        web.setWebViewClient(new Callback());
       // web.loadUrl("https://predictor.glideapp.io/dl/d0a5f4/s/a8e77c/r/LTJHOVAeGuAFPEdDzDQa");
        if (savedInstanceState != null)
            web.restoreState(savedInstanceState);
        else
            web.loadUrl("https://predictor.glideapp.io/dl/d0a5f4/s/a8e77c/r/LTJHOVAeGuAFPEdDzDQa");

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        web.saveState(outState);
        super.onSaveInstanceState(outState);
    }
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
            // By returning false you're telling Android that, this is my website, so, do not override;
            // let WebView load the page.
            return false;
        }

}}