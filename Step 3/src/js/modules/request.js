const request =  ({url, data, options = {}}) =>{
  const key = "50661946-4249-422e-9f67-1272735c4b19";
  return fetch(url, {
    method: "GET",
    ...options,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${key}`
    },
    body: JSON.stringify(data)
  })
}

export default request