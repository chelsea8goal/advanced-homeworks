const data = [
      {
          
          id: "modal-1",
          modalData: {
               title: "Do you want to delete this file?",
               text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
               closeButton: true,
               actions: {
                    ok() {console.log("OK (fist modal)")},
                    cancel() {console.log("CANCEL (first modal)")}
               }
          },
          modalStyles: {
               background: "rgb(231, 76, 60)",
               headerColor: "rgb(212, 70, 55)",
               headerTextColor: "rgb(255, 255, 255)",
               textColor: "rgb(255, 255, 255)",
               buttonBackground: "#rgb(179, 56, 44)",
               buttonTextColor: "rgb(255, 255, 255)"
          }
     },
     {
          id: "modal-2",
          modalData: {
               title: "Do you want to download this file?",
               text: "You can save it to your computer. Are you sure you want to accept it?",
               closeButton: false,
               actions: {
                    ok() {console.log("OK (second modal)")},
                    cancel() {console.log("CANCEL (second modal)")}
               }
          },
          modalStyles: {
               background: "#27a127",
               headerColor: "#33c533",
               headerTextColor: "#141414",
               textColor: "#141414",
               buttonBackground: "#ccffcc",
               buttonTextColor: "#141414"
          }
     }
]

export default data;
