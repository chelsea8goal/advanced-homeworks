import { Component } from 'react';
import './App.scss';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
//import { render } from '@testing-library/react';
import data from '../data/data';

class App extends Component {
     constructor() {
          super() 
          this.state = {
               active: false,
          }
          this.handleClick = this.handleClick.bind(this);
          this.changeActive = this.changeActive.bind(this);
     }
          

     handleClick(event) {
          const modalID = event.target.id;
          const modalDeclaration = data.find(item => item.id === modalID);
          this.setState({
               active: !this.state.active,
               ...modalDeclaration
          });
     }
     
     changeActive() {
          this.setState({active: !this.state.active})
     }

     render() {
          const {active, modalData, modalStyles} = this.state
          
          return (
               <div
                    className='main__btn-container'>
                    <Button
                         text={"Open first modal"}
                         className="main__btn" 
                         backgroundcolor={'#ff0000'} 
                         onClick={this.handleClick}
                          id={'modal-1'}/>
                    <Button
                         text={"Open second modal"}
                         className="main__btn" 
                         backgroundcolor={'#00ff00'} 
                         onClick={this.handleClick} 
                         id={'modal-2'}/>
                    {this.state.active &&
                         <Modal
                              active={active} 
                              changeActive={this.changeActive} 
                              modalData={modalData} 
                              modalStyles={modalStyles} />}
               </div>
          );
     }
}


export default App;
