import { Routes, Route, Navigate } from 'react-router-dom';
import Bag from '../pages/Bag/Bag';
import Catalog from '../pages/Catalog/Catalog';
import Favourite from '../pages/Favourite/Favourite';

function Router({ addToBag, toggleFav, favItems, bagItems, removeFromBag, data, auth, changeAuthentication }) {
     return (
          <Routes>
               <Route exact path='/' element={
                    <Navigate to='/catalog' />
               } />
               <Route path="/catalog" element={
                    <Catalog
                         addToBag={addToBag}
                         toggleFav={toggleFav}
                         favItems={favItems}
                         bagItems={bagItems}
                         data={data}
                    />
               } />
               <Route path='/bag' element={
                    <Bag
                         bagItems={bagItems}
                         removeFromBag={removeFromBag}
                         auth={auth}
                         changeAuthentication={changeAuthentication}
                    />
               } />
               <Route path='/favourite' element={
                    <Favourite
                         favItems={favItems}
                         bagItems={bagItems}
                         addToBag={addToBag}
                         toggleFav={toggleFav}
                    />
               } />
          </Routes>
     )
}

export default Router;