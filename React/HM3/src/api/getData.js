const getData = (url) => {
  const data = fetch(url)
    .then(result => result.json())
    .then(data => {
      return data;
    })
    return data;
}

export default getData;