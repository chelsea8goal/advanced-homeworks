import React, { useEffect, useState } from 'react';
import Header from './components/Header/Header';
import Router from './router/router';
import getData from './api/getData';
import './App.scss';


function App() {
  const [bagItems, setBagItems] = useState(JSON.parse(localStorage.getItem('bagItems')) || []);
  const [favItems, setFavItems] = useState(JSON.parse(localStorage.getItem('favItems')) || []);
  const [data, setData] = useState([]);
  const [auth, setAuth] = useState(false);

  useEffect(() => {
    async function fetchData() {
      let data = await getData('./data/products.json');
      await setData(data)
    }
    fetchData()
  }, []);

  useEffect(() => {
    localStorage.setItem('bagItems', JSON.stringify(bagItems))
    localStorage.setItem('favItems', JSON.stringify(favItems))
  });

  const changeAuthentication = () => {
    setAuth(!auth);
  }

  const addToBag = (event) => {
    const selectedItemID = event.target.dataset.id;
    let newItem;
    data.map(el => {
      if (el.id === parseInt(selectedItemID)) newItem = el
    });
    const addToBag = [...bagItems, newItem];
    setBagItems(addToBag);
  }

  const removeFromBag = (event) => {
    const removedItemID = event.target.dataset.id;
    let removedItem;
    data.map(el => {
      if (el.id === parseInt(removedItemID)) removedItem = el;
    })
    const removeFromBag = bagItems.filter(el => el.id !== removedItem.id);
    setBagItems(removeFromBag);
  }

  const toggleFav = (event) => {
    const favouriteID = event.target.dataset.id;
    let favouriteItem;
    data.map(el => {
      if (el.id === parseInt(favouriteID)) favouriteItem = el;
    })
    const addFav = [...favItems, favouriteItem];
    const removeFav = favItems.filter(el => el.id !== favouriteItem.id);

    const item = favItems.filter(el => el.id === parseInt(favouriteID))
    if (item.length > 0) {
      setFavItems(removeFav);
    } else {
      setFavItems(addFav);
    }
  }

  return (
    <>
      <Header
        bagItemsLength={bagItems.length}
        favItemsLength={favItems.length}
        auth={auth}
        changeAuthentication={changeAuthentication}
      />
      <main className='main'>
        <Router
          addToBag={addToBag}
          toggleFav={toggleFav}
          removeFromBag={removeFromBag}
          favItems={favItems}
          bagItems={bagItems}
          data={data} auth={auth}
          changeAuthentication={changeAuthentication}
        />
      </main>
    </>
  )
}

export default App;
