import React from "react";
import './Header.scss';
import shoppingBag from './shopping-bag-icon.png';
import favourite from './favourite.svg';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function Header({ bagItemsLength, favItemsLength, auth, changeAuthentication }) {
     return (
          <header className="header">
               <div className="header__fixed">
                    <div className="header__box">
                         <Link to="/" className="header__logo">
                              <p>POP Shop</p>
                         </Link>
                         
                         <Link to="bag" className="header__itemBox">
                              <div className="header__bag">
                                   <img src={shoppingBag} alt="shopping bag" />
                              </div>
                              <span className="header__span">{
                                   auth ? `(${bagItemsLength} items)` : false
                              }</span>
                         </Link>

                         <Link to="favourite" className="header__itemBox">
                              <div className="header__favourite">
                                   <img src={favourite} alt="favourite" />
                              </div>
                              <span className="header__span">{
                                   auth ? `(${favItemsLength} items)` : false
                              }</span>
                         </Link>

                         <button className="header__loginBtn" onClick={changeAuthentication}>{auth ? "Logout" : 'Login'}</button>
                    </div>
               </div>
          </header>
     )
}

Header.propTypes = {
     bagItemsLength: PropTypes.number,
     favItemsLength: PropTypes.number,
     auth: PropTypes.bool,
     changeAuthentication: PropTypes.func
}

export default Header;