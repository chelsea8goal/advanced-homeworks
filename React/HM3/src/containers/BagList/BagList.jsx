import React from "react";
import PropTypes from 'prop-types';
import './BagList.scss';
import BagCard from "../../components/BagCard/BagCard";

function BagList({ bagItems, removeFromBag }) {

     return (
          <ul className='BagList'>
               {
                    bagItems.map(el => {
                         const { id, name, price, picture, color, code } = el;

                         return (
                              <li key={id}>
                                   <BagCard
                                        removeFromBag={removeFromBag}
                                        id={id}
                                        name={name}
                                        price={price}
                                        picture={picture}
                                        color={color}
                                        code={code}
                                   />
                              </li>
                         )
                    })
               }
          </ul>
     )
}

BagList.propTypes = {
     bagItems: PropTypes.array,
     removeFromBag: PropTypes.func
}

export default BagList;